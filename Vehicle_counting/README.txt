System Requirement
1. Raspbian OS
2. python 3.6
3. Tensorflow(cpu) 1.14
4. TightVNC
5. Openvino 2019 R3 

Tensorflow installation 		: pip install tensorflow==1.14
Openvino installation step		: httpsdocs.openvinotoolkit.orglatest_docs_install_guides_installing_openvino_raspbian.html
Openvino package download link  : httpsdownload.01.orgopencv2019openvinotoolkitR3

Dependency
Tensorflow models library. Installation step : httpstensorflow-object-detection-api-tutorial.readthedocs.ioenlatestinstall.html#tf-models-install
