ada 2 folder

- folder 'script_ambil_data'
- folder 'script_web_server'

Untuk folder 'script_ambil_data', ada 3 script yaitu 'data.py', 'feature.py', dan 'gui.py'.
Untuk menjalankan script GUI, jalankan 'gui.py', kemudian pilih "Ambil Data". Jika sudah selesai, pilih "Ekstraksi Ciri". Jika sudah selesai, close program.
Untuk menjalankan tanpa GUI, jalankan terlebih dahulu 'data.py' baru setelah itu jalankan 'feature.py'.

Untuk Ambil Data Wajah :

- Tekan 'N' untuk mendaftarkan nama baru.
- Input nama pada Terminal
- Sesuaikan wajah sehingga kotak masuk ke frame.
- Tekan 'S' untuk menyimpan citra wajah. Lakukan berulang kali untuk mendapatkan citra wajah yang lebih banyak.
- Jika sudah cukup, bisa tekan 'N' lagi untuk mendaftar wajah baru atau tekan 'Q' untuk keluar.

Untuk folder 'script_web_server', letakkan semua yang ada di dalam folder tersebut kedalam direktori web server.
Sebelum dipakai, buat database telebih dahulu menggunakan phpmyadmin, kemudian sesuaikan dengan program PHP nya.