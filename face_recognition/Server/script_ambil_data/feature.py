# Ekstraksi ciri dari citra wajah dan nanti disimpan ke file features_all.csv

# Author:   coneypo
# Blog:     http://www.cnblogs.com/AdaminXie
# GitHub:   https://github.com/coneypo/Dlib_face_recognition_from_camera
# Mail:     coneypo@foxmail.com

import cv2
import os
import dlib
from skimage import io
import csv
import numpy as np
import matplotlib

# direktorinya diganti ke direktori web server
path_images_from_camera = "/var/www/html/Web_server/data_faces_from_camera/"

# memanggil face detector dari library dlib
detector = dlib.get_frontal_face_detector()

# memanggil library shape predictor
predictor = dlib.shape_predictor("data/data_dlib/shape_predictor_68_face_landmarks.dat")


# Model Face Recognition, modelnya nanti akan memetakan wajahnya menjadi vektor 128D
face_rec = dlib.face_recognition_model_v1("data/data_dlib/dlib_face_recognition_resnet_model_v1.dat")


def return_128d_features(path_img):
    img_rd = io.imread(path_img,plugin='matplotlib')
    img_gray = cv2.cvtColor(img_rd, cv2.COLOR_BGR2RGB)
    faces = detector(img_gray, 1)

    print("%-40s %-20s" % ("foto dengan wajah yang terdeteksi:", path_img), '\n')

    if len(faces) != 0:
        shape = predictor(img_gray, faces[0])
        face_descriptor = face_rec.compute_face_descriptor(img_gray, shape)
    else:
        face_descriptor = 0
        print("tidak ada wajah.")

    return face_descriptor


def return_features_mean_personX(path_faces_personX):
    features_list_personX = []
    photos_list = os.listdir(path_faces_personX)
    if photos_list:
        for i in range(len(photos_list)):
            print("%-40s %-20s" % ("Membaca foto dari:", path_faces_personX + "/" + photos_list[i]))
            features_128d = return_128d_features(path_faces_personX + "/" + photos_list[i])
            if features_128d == 0:
                i += 1
            else:
                features_list_personX.append(features_128d)
    else:
        print("Perhatian : tidak ada wajah di " + path_faces_personX + '/', '\n')

    if features_list_personX:
        features_mean_personX = np.array(features_list_personX).mean(axis=0)
    else:
        features_mean_personX = '0'

    return features_mean_personX


# get the num of latest person
person_list = os.listdir("/var/www/html/Web_server/data_faces_from_camera/") #diganti ke direktori web server
person_num_list = []
for person in person_list:
    person_num_list.append(int(person.split('_')[-1]))
person_cnt = max(person_num_list)

with open("/var/www/html/Web_server/features_all.csv", "w", newline="") as csvfile: #diganti ke direktori webserver
    writer = csv.writer(csvfile)
    for person in range(person_cnt):
        # Get the mean/average features of face/personX, it will be a list with a length of 128D 
        print(path_images_from_camera + "person_"+str(person+1))
        features_mean_personX = return_features_mean_personX(path_images_from_camera + "person_"+str(person+1))
        writer.writerow(features_mean_personX)
        print("Rerata ciri:", list(features_mean_personX))
        print('\n')
    print("Menyimpan semua ciri ke dalam folder: data/features_all.csv")
