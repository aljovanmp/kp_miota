#Program ini khusus kalo camera nya pake Pi Camera.
from picamera.array import PiRGBArray
from picamera import PiCamera
import dlib         
import numpy as np  
import cv2          
import os           
import shutil       
import csv
from pathlib import Path

detector = dlib.get_frontal_face_detector()
cap = PiCamera()
cap.resolution = (320, 240)
cap.framerate = 30
rawCapture = PiRGBArray(cap, size=(320, 240))
cnt_ss = 0
current_face_dir = ""
path_photos_from_camera = "/var/www/html/Web_server/data_faces_from_camera/" #Ganti pathnya ke direktori web server

daftar = []
path = Path()
result = list(path.glob('/var/www/html/Web_server/name.csv')) #Ganti pathnya ke direktori web server
if len(result) == 1:
    with open("/var/www/html/Web_server/name.csv", "r") as g: #Ganti pathnya ke direktori web server
        gw = csv.reader(g, delimiter=',')
        for lines in gw:
            panjang = len(lines)
            for i in lines:
               daftar.append(i)
else:
    pippo = []

def pre_work_mkdir():
    if os.path.isdir(path_photos_from_camera):
        pass
    else:
        os.mkdir(path_photos_from_camera)

pre_work_mkdir()

def pre_work_del_old_face_folders():
    folders_rd = os.listdir(path_photos_from_camera)
    for i in range(len(folders_rd)):
        shutil.rmtree(path_photos_from_camera+folders_rd[i])

    if os.path.isfile("/var/www/html/Web_server/features_all.csv"): #Ganti pathnya ke direktori web server
        os.remove("/var/www/html/Web_server/features_all.csv") #Ganti pathnya ke direktori web server

# pre_work_del_old_face_folders() #kalo mau ngehapus semua data wajah sebelumnya (reset)

if os.listdir("/var/www/html/Web_server/data_faces_from_camera/"): #Ganti pathnya ke direktori web server
    person_list = os.listdir("/var/www/html/Web_server/data_faces_from_camera/") #Ganti pathnya ke direktori web server
    person_num_list = []
    for person in person_list:
        person_num_list.append(int(person.split('_')[-1]))
    person_cnt = max(person_num_list)

else:
    person_cnt = 0

save_flag = 1

press_n_flag = 0

for frame in cap.capture_continuous(rawCapture, format="bgr", use_video_port=True):
    img_rd = frame.array
    kk = cv2.waitKey(1) & 0xFF
    rawCapture.truncate(0)
    if kk == ord("q"):
        break
    img_gray = cv2.cvtColor(img_rd, cv2.COLOR_BGR2GRAY)
    faces = detector(img_gray, 0)
    font = cv2.FONT_ITALIC
    if kk == ord('n'):
        person_cnt += 1
        nama = input("Input Nama: ")
        current_face_dir = path_photos_from_camera + "person_" + str(person_cnt)
        os.makedirs(current_face_dir)
        daftar.append(nama)
        print('\n')
        print("Membuat folder / Create folders: ", current_face_dir)
        print('\n')
        print(nama)

        cnt_ss = 0
        press_n_flag = 1

    # Setelah muka terdeteksi
    if len(faces) != 0:
        # Generate kotak wajah
        for k, d in enumerate(faces):
            # Menghitung panjang dan lebar kotaknya
            # (x,y), (width, height)
            pos_start = tuple([d.left(), d.top()])
            pos_end = tuple([d.right(), d.bottom()])

            # Menghitung ukuran box
            height = (d.bottom() - d.top())
            width = (d.right() - d.left())

            hh = int(height/2)
            ww = int(width/2)

            # Warna kotaknya
            color_rectangle = (255, 255, 255)

            # Resolusi kameranya 320x240
            if (d.right()+ww) > 320  or (d.bottom()+hh > 240) or (d.left()-ww < 0) or (d.top()-hh < 0):
                cv2.putText(img_rd, "KOTAK KELUAR FRAME!", (20, 120), font, 0.8, (0, 0, 255), 1, cv2.LINE_AA)
                color_rectangle = (0, 0, 255)
                save_flag = 0
                if kk == ord('s'):
                    print("Pastikan kotak di dalam bingkai / Please adjust your position")
            else:
                color_rectangle = (255, 255, 255)
                save_flag = 1

            cv2.rectangle(img_rd,
                          tuple([d.left() - ww, d.top() - hh]),
                          tuple([d.right() + ww, d.bottom() + hh]),
                          color_rectangle, 2)

            # Create blank image according to the shape of face detected
            im_blank = np.zeros((int(height*2), width*2, 3), np.uint8)

            if save_flag:
                # Program untuk menyimpan foto saat menekan 'S'
                if kk == ord('s'):
                    # Cek apakah sudah nekan N terlebih dahulu atau belum
                    if press_n_flag:
                        cnt_ss += 1
                        for ii in range(height*2):
                            for jj in range(width*2):
                                im_blank[ii][jj] = img_rd[d.top()-hh + ii][d.left()-ww + jj]
                        cv2.imwrite(current_face_dir + "/img_face_" + str(cnt_ss) + ".jpg", im_blank)
                        print("Tersimpan di / Save into: ", str(current_face_dir) + "/img_face_" + str(cnt_ss) + ".jpg")
                    else:
                        print("Tekan 'N' terlebih dahulu / Please press 'N' before 'S'")

    # Cek sudah berapa wajah yang masuk database
    cv2.putText(img_rd, "Wajah: " + str(len(faces)), (20, 50), font, 0.3, (0, 255, 0), 1, cv2.LINE_AA)

    # Keterangan pada window di layar
    cv2.putText(img_rd, "Data Wajah Baru", (20, 20), font, 0.5, (0, 0, 0), 1, cv2.LINE_AA)
    cv2.putText(img_rd, "N: Daftarkan Wajah Baru", (20, 180), font, 0.3, (0, 0, 0), 1, cv2.LINE_AA)
    cv2.putText(img_rd, "S: Tangkap Citra Wajah", (20, 200), font, 0.3, (0, 0, 0), 1, cv2.LINE_AA)
    cv2.putText(img_rd, "Q: Keluar", (20, 220), font, 0.3, (0, 0, 0), 1, cv2.LINE_AA)

    # 6. Tekan 'Q' untuk keluar
    if kk == ord('q'):
        break
    # Uncomment this line if you want the camera window is resizeable
    # cv2.namedWindow("camera", 0)

    cv2.imshow("camera", img_rd)

# menyimpan nama ke file name.csv
f = open('/var/www/html/Web_server/name.csv', 'w')
with f:
    fw = csv.writer(f)
    fw.writerow(daftar)
cap.close()
cv2.destroyAllWindows()
