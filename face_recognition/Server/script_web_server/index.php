<?php 
    include('connect.php');
?>
<html>
<head>
    <link rel="icon" href="yugioh.png">
    <title>Database Absensi Wajah</title>
    <style type="text/css">
        .table_titles, .table_cells_odd, .table_cells_even {
                padding-right: 20px;
                padding-left: 20px;
                color: #000;
        }
        .table_titles {
            color: #FFF;
            background-color: #1F456E;
        }
        .table_cells_odd {
            background-color: #CCC;
        }
        .table_cells_even {
            background-color: #FAFAFA;
        }
        table {
            border: 2px solid #000;
            background-color: #FFF;
            opacity: 1;
            margin-left:auto;
            margin-right:auto;
            border-radius: 5px;
            padding: 1px;
        }
        body { 
          font-family: "Trebuchet MS", Arial;
          background-image: url('imagee.jpg');
          background-color: #48AAAD;
          background-repeat: no-repeat;
          background-attachment: fixed;
          background-size: 100% 100%;
          opacity: 1;
        }
        
    </style>
</head>
    <body>
        <h1 style="text-align:center;color:#FFFFFF;">Data Absensi Berbasis Wajah</h1>
    <table border="1" cellspacing="0" cellpadding="5" table style="margin-left:auto;margin-right:auto;">
      <tr>
            <td class="table_titles">IP Address</td>
            <td class="table_titles">Nama</td>
            <td class="table_titles">Tanggal dan Waktu</td>
          </tr>
<?php

    $sql = "SELECT * FROM SensorData";
    $result = mysqli_query($conn, $sql) or die("Bad Query: $sql");
    $oddrow = true;
    while( $row = mysqli_fetch_array($result))
    {
        if ($oddrow) 
        { 
            $css_class=' class="table_cells_odd"'; 
        }
        else
        { 
            $css_class=' class="table_cells_even"'; 
        }
        $oddrow = !$oddrow;
        echo "<tr><td>{$row['ID']}</td><td>{$row['Name']}</td><td>{$row['DateAndTime']}</td></tr>";
    }
?>
