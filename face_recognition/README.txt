System Requirements untuk Client dan Server:

1. PiCamera
2. Dlib
3. OpenCV 4.0
4. Pandas
5. Urllib.request
6. Netifaces 
7. Python 3.7
8. Scikit-image
9. Scipy

System Requirements khusus untuk Server:

1. Apache Web Server
2. phpmyadmin
3. PHP 7.4
4. MySQL