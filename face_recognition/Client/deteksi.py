#Program ini berlaku untuk Pi Camera.
from picamera.array import PiRGBArray
from picamera import PiCamera
import dlib          #Dlib
import numpy as np   #numpy
import cv2           #OpenCv
import pandas as pd  #Pandas
import os
import csv
import urllib.request
import netifaces as ni

#model face recognitionnya
facerec = dlib.face_recognition_model_v1("data/data_dlib/dlib_face_recognition_resnet_model_v1.dat")

#ambil ip client
ni.ifaddresses('wlan0')
ip = ni.ifaddresses('wlan0')[ni.AF_INET][0]['addr']

#download dari server
url = 'http://192.168.43.218/Web_server/name.csv' #direktori name.csv di web server
urllib.request.urlretrieve(url, 'data/name.csv') #name.csv harus ada di dalam folder data
url2 = 'http://192.168.43.218/Web_server/features_all.csv' #direktori features_all.csv di web server
urllib.request.urlretrieve(url2, 'data/features_all.csv') #features_all.csv harus ada di dalam folder data

# Ambil data dari csv database daftar wajah
daftar = []
with open("data/name.csv", "r") as g:
    gw = csv.reader(g, delimiter=',')
    for lines in gw:
        panjang = len(lines)
        for i in lines:
           daftar.append(i)

# Menghitung e-distance diantara dua ciri 128D
def return_euclidean_distance(feature_1, feature_2):
    feature_1 = np.array(feature_1)
    feature_2 = np.array(feature_2)
    dist = np.sqrt(np.sum(np.square(feature_1 - feature_2)))
    return dist


# 1. Cek fitur di csv
if os.path.exists("data/features_all.csv"):
    path_features_known_csv = "data/features_all.csv"
    csv_rd = pd.read_csv(path_features_known_csv, header=None)

    # Array untuk menyimpan ciri wajah
    features_known_arr = []

    # Menampilkan jumlah wajah di database
    for i in range(csv_rd.shape[0]):
        features_someone_arr = []
        for j in range(0, len(csv_rd.ix[i, :])):
            features_someone_arr.append(csv_rd.ix[i, :][j])
        features_known_arr.append(features_someone_arr)
    print("Jumlah wajah di database：", len(features_known_arr))

    # Memanggil algoritma deteksi wajah
    detector = dlib.get_frontal_face_detector()
    predictor = dlib.shape_predictor('data/data_dlib/shape_predictor_68_face_landmarks.dat')

    # Mengaktifkan Pi Camera
    cap = PiCamera()
    cap.resolution = (320, 240)
    cap.framerate = 30
    rawCapture = PiRGBArray(cap, size=(320, 240))

    # 3. Menangkap citra dan konversi ke greyscale
    for frame in cap.capture_continuous(rawCapture, format="bgr", use_video_port=True):

        img_rd = frame.array
        kk = cv2.waitKey(1) & 0xFF
        rawCapture.truncate(0)
 
	# if the `q` key was pressed, break from the loop
        if kk == ord("q"):
            break
        img_gray = cv2.cvtColor(img_rd, cv2.COLOR_RGB2GRAY)
        faces = detector(img_gray, 0)

        # font to write later
        font = cv2.FONT_ITALIC

        # Array untuk menampilkan nama dan posisi wajah pada frame
        pos_namelist = []
        name_namelist = []
        kk = cv2.waitKey(1)

        # Perintah Q untuk keluar
        if kk == ord('q'):
            break
        elif kk == ord('p'):
            #download dari server
            url = 'http://192.168.43.218/Web_server/name.csv'
            urllib.request.urlretrieve(url, 'data/name.csv')
            url2 = 'http://192.168.43.218/Web_server/features_all.csv'
            urllib.request.urlretrieve(url2, 'data/features_all.csv')
            
            # Ambil data dari csv database daftar wajah
            daftar = []
            with open("data/name.csv", "r") as g:
                gw = csv.reader(g, delimiter=',')
                for lines in gw:
                    panjang = len(lines)
                    for i in lines:
                       daftar.append(i)
            
            # Menghitung e-distance diantara dua ciri 128D
            def return_euclidean_distance(feature_1, feature_2):
                feature_1 = np.array(feature_1)
                feature_2 = np.array(feature_2)
                dist = np.sqrt(np.sum(np.square(feature_1 - feature_2)))
                return dist
            
            
            # 1. Cek fitur di csv
            if os.path.exists("data/features_all.csv"):
                path_features_known_csv = "data/features_all.csv"
                csv_rd = pd.read_csv(path_features_known_csv, header=None)
            
                # Array untuk menyimpan ciri wajah
                features_known_arr = []
            
                # Menampilkan jumlah wajah di database
                for i in range(csv_rd.shape[0]):
                    features_someone_arr = []
                    for j in range(0, len(csv_rd.ix[i, :])):
                        features_someone_arr.append(csv_rd.ix[i, :][j])
                    features_known_arr.append(features_someone_arr)
                print("Jumlah wajah di database：", len(features_known_arr))
            
                # Memanggil algoritma deteksi wajah
                detector = dlib.get_frontal_face_detector()
                predictor = dlib.shape_predictor('data/data_dlib/shape_predictor_68_face_landmarks.dat')
        else:
            # jika wajah dideteksi
            if len(faces) != 0:
                # 4. Menangkap ciri dari wajah yang terdeteksi
                features_cap_arr = []
                for i in range(len(faces)):
                    shape = predictor(img_rd, faces[i])
                    features_cap_arr.append(facerec.compute_face_descriptor(img_rd, shape))

                # 5. Mentraversalkan semua wajah di database
                for k in range(len(faces)):
                    print("##### camera person", k+1, "#####")
                    # Secara default wajah diberi nama "unknown"
                    name_namelist.append("Tak dikenal")

                    # Memasukkan parameter posisi dari wajah
                    pos_namelist.append(tuple([faces[k].left(), int(faces[k].bottom() + (faces[k].bottom() - faces[k].top())/4)]))

                    # Membandingkan wajah tertangkap dengan database
                    e_distance_list = []
                    for i in range(len(features_known_arr)):
                        if str(features_known_arr[i][0]) != '0.0':
                            print("with person", str(i + 1), "the e distance: ", end='')
                            e_distance_tmp = return_euclidean_distance(features_cap_arr[k], features_known_arr[i])
                            print(e_distance_tmp)
                            e_distance_list.append(e_distance_tmp)
                        else:
                            e_distance_list.append(999999999)
                    # Mencari minimum e distance
                    similar_person_num = e_distance_list.index(min(e_distance_list))
                    print("Minimum e distance with person", int(similar_person_num)+1)

                    if min(e_distance_list) < 0.4:
                        # jika dibawah e-distance kurang dari 0.4, nilai terkecilnya akan dideteksi sebagai orang ke-k.
                        # jika tidak, akan dideteksi sebagai "tak dikenal"
                        name_namelist[k] = daftar[int(similar_person_num)]
                        #memasukkan hasil deteksi ke database SQL server
                        urllib.request.urlopen('http://192.168.43.218/add_data.php?id='+ip+'&name='+name_namelist[k])
                        print("May be "+name_namelist[k])
                    else:
                        print("Unknown person")

                    # Membentuk persegi empat di wajah
                    for kk, d in enumerate(faces):
                        cv2.rectangle(img_rd, tuple([d.left(), d.top()]), tuple([d.right(), d.bottom()]), (0, 255, 255), 2)
                    print('\n')
                    
                # 6. Menampilkan nama di bawah kotak
                for i in range(len(faces)):
                    cv2.putText(img_rd, str(name_namelist[i]), pos_namelist[i], font, 0.4, (0, 255, 255), 1, cv2.LINE_AA)

        print("Faces in camera now:", name_namelist, "\n")

        cv2.putText(img_rd, "Tekan 'Q': Keluar", (200, 200), font, 0.4, (84, 255, 159), 1, cv2.LINE_AA)
        cv2.putText(img_rd, "Deteksi Wajah", (20, 40), font, 0.8, (0, 0, 0), 1, cv2.LINE_AA)
        cv2.putText(img_rd, "Wajah: " + str(len(faces)), (20, 200), font, 0.5, (0, 0, 255), 1, cv2.LINE_AA)

        cv2.imshow("camera", img_rd)
        

    cap.close()
    cv2.destroyAllWindows()

else:
    print('##### Warning #####', '\n')
    print("'features_all.py' not found!")
    print("Please run 'get_faces_from_camera.py' and 'features_extraction_to_csv.py' before 'face_reco_from_camera.py'", '\n')
    print('##### Warning #####')
